# SimEFB Data Package

## Editing this package

If you come across this data package, it has been downloaded by the SimEFB app.
This package itself should not be edited, as it will be overwritten with any
data updates. If you wish to insert custom data into your SimEFB installation,
please make a folder alongside the `data` folder called `custom` using the same
layout, as shown below:

```
SimEFB
 ├─ data - Containing the official data package
 │   └─ ...
 └─ custom - Containing any custom data
     └─ ...
```

## Layout

The folder layout is as below:

```
data
 ├─ aerodromes - Containing all data relating to aerodromes
 │   └─ aips - Containing indexes of global AIPs
 └─ navaids - Contains all data relating to navigational aids
```

## `aerodromes`

The `aerodromes` folder contains all data relating to aerodromes globally.

### `aerodromes/icao.txt`

`icao.txt` contains a list of all global ICAO codes and the relevant aerodrome
name for quick conversion. `icao.txt` is in the following format:

```
ICAO,Airport Name
```

For example:

```
EGLL,LONDON/HEATHROW
```

Only one airport should be on each line. Line endings should be in the `LF`
format.

### `aerodromes/aips/*.json`

The AIPs folder contains JSON files with indexes of global AIP providers. Local
AIPs should be named `10-AIPNAME.json`, for example `10-nats.json`. Global AIPs
should be named `50-AIPNAME.json`. This ensures that government AIPs are
prioritised over old charts. These should be in the format:

```json
{
    "EGLL": {
        "Aerodrome Chart": "URL to Aerodrome Chart"
    }
}
```

A special case has been provided for NATS URLs, which change daily. The sequence
`%n` will be replaced with the daily NATS hash.

